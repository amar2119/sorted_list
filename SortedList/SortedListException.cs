﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedList
{
    class SortedListException : Exception
    {
       SortedListException (String message) : base(message)
        {

        }
    }
}
